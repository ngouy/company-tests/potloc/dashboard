import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';

export default Component.extend({
  store: service(),

  selected_shoes: [],
  selected_stores: [],

  init() {
    this._super(...arguments);
    this.updateData();
  },

  // TODO : debounce this
  updateData() {
    const store_names = this.get('selected_stores').join(','),
          shoe_names  = this.get('selected_shoes').join(',');
    this.get('store')
        .queryRecord('stat', {
          me: true,
          filter: {
            store_names,
            shoe_names
          }
        }).then(data => {
          this.set('data', data);
        })
    ;
  },

  totalAmmount: computed('data.{stock_up,stock_down}', function () {
    return this.get('data.stock_up') + this.get('data.stock_down');
  }),
  totalMoney: computed('data.{incomes,expenses}', function () {
    return this.get('data.incomes') + this.get('data.expenses');
  }),
  intervalTime: computed('data.{start_date,end_date}', function() {
    return (this.get('data.end_date') - this.get('data.start_date')) / 1000;
  }),


  actions: {

    filtersChanged(filters) {
      this.set('selected_shoes',  filters.selected_shoes);
      this.set('selected_stores', filters.selected_stores);
      this.updateData();
    },

    refreshData() {
      this.updateData();
    },

  },

});