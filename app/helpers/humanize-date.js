import Helper from '@ember/component/helper';

export default Helper.extend({
  compute([seconds]) {

    const year = Math.floor(seconds / (3600 * 24 * 30.5 * 12));
    seconds -= year * 3600 * 24 * 30.5 * 12;
    const month = Math.floor(seconds / (3600 * 24 * 30.5));
    seconds -= month * 3600 * 24 * 30.5;
    const days = Math.floor(seconds / (3600 * 24));
    seconds -= days * 3600 * 24;
    const hours = Math.floor(seconds / 3600);
    seconds -= hours * 3600;
    const minutes = Math.floor(seconds / 60);
    seconds -= minutes * 60;

    if (year    > 0) { return year    + ' years and '   + month   + ' months'                             ; }
    if (month   > 0) { return month   + ' months and '  + days    + ' days'                               ; }
    if (days    > 0) { return days    + ' days and '    + hours   + ' hours'                              ; }
    if (hours   > 0) { return hours   + ' hours and '   + minutes + ' minutes and ' + seconds + ' seconds'; }
    if (minutes > 0) { return minutes + ' minutes and ' + seconds + ' seconds'                            ; }
    if (seconds > 0) { return seconds + ' seconds'                                                        ; }
  }
});