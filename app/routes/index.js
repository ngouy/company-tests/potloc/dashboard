import Route from '@ember/routing/route';

export default Route.extend({

  // root
  redirect: function () {
    this.transitionTo('live-stats');
  }

});