import utils from 'shoes-dashboard/lib/my-utils';
import Component from '@ember/component';
import { computed, get, observer, set } from '@ember/object';
import { inject as service } from '@ember/service';

export default Component.extend({
  store: service(),

  classNames: ['live-stats-page', 'ui basic segment'],

  interval_time: 2,
  stacked_time: 0,

  selected_shoes: [],
  selected_stores: [],

  init() {
    this._super(...arguments);
    this.set('begin_stack_time', this.timeNow());
    this.set('last_end_date',    this.timeNow());
    this.updateData();
    this.setCountDown();
  },

  // data

  // TODO : debounce this
  updateData() {
    const now         = this.timeNow(),
          store_names = this.get('selected_stores').join(','),
          shoe_names  = this.get('selected_shoes').join(',');
    const promise     = this.get('store').queryRecord('stat', {
      me: true,
      filter: {
        start_date: this.get('last_end_date'),
        end_date: now,
        store_names,
        shoe_names
      }
    });
    promise.then(data => {
      this.set('updatedData', data);
    });
    this.set('last_end_date', now);
    this.set('current_countdown', this.get('interval_time'));
    return promise;
  },

  stackedDataUpdated: observer('updatedData', function () {
    const { stackedData, updatedData } = this.getProperties(['stackedData', 'updatedData']);
    if (!stackedData || this.get('reset_stacked_data')) {
      this.set('reset_stacked_data', false);
      this.set('stackedData', updatedData);
      return;
    }
    ['stock_up',
    'stock_down',
    'incomes',
    'expenses',
    'transactions_in',
    'transactions_out'].forEach(key => {
      set(stackedData, key, get(stackedData, key) + updatedData.get(key));
    });
    this.set('stackedData', stackedData);
    this.set('stacked_time', this.timeNow() - this.get('begin_stack_time'));
  }),

  totalAmmount: computed('updatedData.{stock_up,stock_down}', function () {
    return this.get('updatedData.stock_up') + this.get('updatedData.stock_down');
  }),
  totalMoney: computed('updatedData.{incomes,expenses}', function () {
    return this.get('updatedData.incomes') + this.get('updatedData.expenses');
  }),

  stackedAmmount: computed('stackedData.{stock_up,stock_down}', function () {
    return this.get('stackedData.stock_up') + this.get('stackedData.stock_down');
  }),
  stackedMoney: computed('stackedData.{incomes,expenses}', function () {
    return this.get('stackedData.incomes') + this.get('stackedData.expenses');
  }),

  // countdowns

  timeNow() {
    return Math.round(Date.now() / 1000);
  },

  deacreseCountdown() {
    const countdown = utils.min([this.get('interval_time') - 1, this.get('current_countdown') - 1]);
    if (countdown <= 0) {
      this.updateData();
    } else {
      this.set('current_countdown', countdown);
    }
  },

  setCountDown: function () {
    this.set('interval', setInterval(() => {
      this.deacreseCountdown();
    }, 1000));
  },

  willDestroyElement() {
    clearInterval(this.get('interval'));
  },

  actions: {

    filtersChanged(filters) {
      this.set('selected_shoes',  filters.selected_shoes);
      this.set('selected_stores', filters.selected_stores);
      // reset stacked
      this.set('reset_stacked_data', true);
      this.set('begin_stack_time', this.timeNow());
      // update data
      this.set('current_countdown', 0);
    }

  }

});