export default {
  min(array) {
    let result = array[0];
    array.forEach(elem => {
      if (result > elem) {
        result = elem;
      }
    });
    return result;
  },
}