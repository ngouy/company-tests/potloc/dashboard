import DS from 'ember-data';
export default DS.JSONAPIAdapter.extend({
  namespace: 'api',
  host: 'http://localhost:4241',

  // query singleton resources
  urlForQueryRecord(query) {
    let url = this._super(...arguments);
    if (query.me === true) {
      url += '/me';
    }
    delete query.me;
    return url;
  }
});