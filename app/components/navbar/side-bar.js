import Component from '@ember/component';

export default Component.extend({
  classNames: ['ui sidebar vertical left fixed', 'inverted', 'visible', 'labeled icon menu']
});