import Component from '@ember/component';
import { observer } from '@ember/object';
import Chart from 'chart.js';
import StreamingChart from 'chartjs-plugin-streaming';

const colors = {
  expenses:    "219, 40, 40",
  incomes:     "33, 186, 69",
  stock_down:  "146, 191, 177",
  stock_up:    "251, 189, 8"
};

export default Component.extend({
  classNames: ['price-quantity-chart', 'ui segment'],

  didInsertElement() {
    const canvas = this.$('canvas')[0].getContext('2d');

    this.set('chart', new Chart(canvas, {
      type: 'line',
      plugins: StreamingChart,
      data: {
        datasets: [{
          label: 'sales',
          id: 'stock_down',
          yAxisID: 'quantities',
          borderColor: `rgba(${colors.stock_down})`,
          backgroundColor: `rgba(${colors.stock_down}, 0.0)`,
          data: [],
          borderDash: [2, 3]
        }, {
          label: 'purchases',
          id: 'stock_up',
          yAxisID: 'quantities',
          borderColor: `rgba(${colors.stock_up})`,
          backgroundColor: `rgba(${colors.stock_up}, 0.0)`,
          data: [],
          borderDash: [2, 3]
        }, {
          label: 'gain',
          id: 'incomes',
          yAxisID: 'money',
          borderColor: `rgba(${colors.incomes})`,
          backgroundColor: `rgba(${colors.incomes}, 0.05)`,
          data: []
        }, {
          label: 'loss',
          id:  'expenses',
          yAxisID: 'money',
          borderColor: `rgba(${colors.expenses})`,
          backgroundColor: `rgba(${colors.expenses}, 0.05)`,
          data:[]
        }]
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        scales: {
          yAxes: [{
            id: 'quantities',
            type: 'linear',
            position: 'left',
            scaleLabel: {
              labelString: 'stocks',
              display: true
            }
          }, {
            id: 'money',
            type: 'linear',
            position: 'right',
            scaleLabel: {
              labelString: 'cash flow',
              display: true
            }
          }],
          xAxes: [{
            type: 'realtime'
          }]
        },
        plugins: {
          streaming: {
            delay: 5000,
            duration: 60 * 1000
          }
        }
      }
    }));
  },

  dataObserver: observer('data', function () {
    const { data, chart } = this.getProperties(['data', 'chart']),
          now = Date.now();
    chart.data.datasets.forEach(dataset => {
      dataset.data.push({
        y: Math.abs(data.get(dataset.id)),
        x: now
      });
    });
    chart.update();
  })

});