import DS from 'ember-data';

const { attr, Model } = DS;

export default Model.extend({
    store_name: attr({ read_only: true }),
    shoe_name:  attr({ read_only: true }),
    stock:      attr('number', { read_only: true }),
});