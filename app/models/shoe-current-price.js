import DS from 'ember-data';

const { attr, Model } = DS;

export default Model.extend({
  shoe_name:     attr({ read_only: true }),
  selling_price: attr('number'),
  buying_price:  attr('number')
});