import Helper from "@ember/component/helper";

export default Helper.extend({
  compute([number]) {
    return this.formatNumber(number);
  },

  formatNumber(number) {
    const direction = number < 0 ? -1 : 1;
    number = Math.abs(number);
    if (isNaN(number)) { return; }
    if (number < 1000) {
      return number * direction;
    } else if (number < 100000) {
      return `${Math.round(number / 100) / 10 * direction}k`;
    } else if (number < 100000000) {
      return `${Math.round(number / 100000) / 10 * direction}M`;
    } else if (number < 100000000000) {
      return `${Math.round(number / 100000000) / 10 * direction}MM`;
    } else {
      return this.formatNumber((number / 100000000) / 10 * direction) + ' MM';
    }
  }
});