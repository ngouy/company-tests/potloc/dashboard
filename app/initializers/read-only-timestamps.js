import DS from 'ember-data';

const { attr, Model } = DS;

export default {
  name: 'readon-only-timestamps',
  initialize() {
    Model.reopen({
      created_at: attr('date', { read_only: true }),
      updated_at: attr('date', { read_only: true })
    });
  }
};