import DS from 'ember-data';

const { attr, Model } = DS;

// TODO : ReadOnlyModel
export default Model.extend({
  stock_up:         attr('number', { read_only: true }),
  stock_down:       attr('number', { read_only: true }),
  incomes:          attr('number', { read_only: true }),
  expenses:         attr('number', { read_only: true }),
  transactions_in:  attr('number', { read_only: true }),
  transactions_out: attr('number', { read_only: true }),
  start_date:       attr('date',   { read_only: true }),
  end_date:         attr('date',   { read_only: true }),
});