import Component from "@ember/component";
import { inject as service } from '@ember/service';

export default Component.extend({
  ember_store: service('store'),

  classNames: ['ui', 'basic', 'segment', 'two column grid'],
  shoes: [],
  selected_shoes: [],
  stores: [],
  selected_stores: [],

  init() {
    Promise.all([
      this.search('stores'),
      this.search('shoes')
    ]).then(() => this.didRender());
    this._super(...arguments);
  },

  didRender() {
    const self = this;
    const base = {
      onAdd: function () {
        // keep both context
        return self.onChange('add', this, ...arguments);
      },
      onRemove: function () {
        return self.onChange('remove', this, ...arguments);
      },
      onNoResults: function () {
        return self.search(this, ...arguments);
      }
    };
    ['stores', 'shoes'].forEach(key => {
      this.$(`.ui.dropdown.${key}`).dropdown(Object.assign(base, {
        values: this.getValues(key),
        placeholder: key
      }));
    });
  },

  getValues(key) {
    const values = this.get(key),
          selected = this.get(`selected_${key}`);
    return values.map(v => {
      return {
        name: v,
        value: v,
        selected: selected.indexOf(v) !== -1
      };
    });
  },

  onChange(direction, elem, new_value) {
    this.get(`selected_${elem.firstElementChild.name}`)[`${direction}Object`](new_value);
    let onChangeParams = {};
    ['shoes', 'stores'].map(k => {
      k = `selected_${k}`;
      onChangeParams[k] = this.get(k);
    });
    this.get('onUpdate')(onChangeParams);
    return true;
  },

  search(elem) {
    const key = typeof elem === "string" ? elem : elem.name || elem.firstElementChild.name;
    return this.ember_store
               .findAll('current-stock')
               .then(elems => {
                this.set(
                  key,
                  // singularized_key + _name
                  elems.mapBy(`${key.slice(0,-1)}_name`)
                       .uniq()
                )
               })
    ;
  }
});