import DS from 'ember-data';

export default {
  name: 'jsonapi-serializer-readonly-attr',
  initialize() {
    DS.JSONAPISerializer.reopen({
      serializeAttribute(snapshot, json, key, attribute) {
        if (attribute.options && attribute.options.read_only) {
          return;
        }
        this._super(...arguments);
      }
    });
  }
};